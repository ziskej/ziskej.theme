# -*- coding: utf-8 -*-
"""Setup tests for this package."""
from plone import api
from ziskej.theme.testing import ZISKEJ_THEME_INTEGRATION_TESTING  # noqa

import unittest


class TestSetup(unittest.TestCase):
    """Test that ziskej.theme is properly installed."""

    layer = ZISKEJ_THEME_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_product_installed(self):
        """Test if ziskej.theme is installed."""
        self.assertTrue(self.installer.isProductInstalled(
            'ziskej.theme'))

    def test_browserlayer(self):
        """Test that IZiskejThemeLayer is registered."""
        from ziskej.theme.interfaces import (
            IZiskejThemeLayer)
        from plone.browserlayer import utils
        self.assertIn(IZiskejThemeLayer, utils.registered_layers())


class TestUninstall(unittest.TestCase):

    layer = ZISKEJ_THEME_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')
        self.installer.uninstallProducts(['ziskej.theme'])

    def test_product_uninstalled(self):
        """Test if ziskej.theme is cleanly uninstalled."""
        self.assertFalse(self.installer.isProductInstalled(
            'ziskej.theme'))

    def test_browserlayer_removed(self):
        """Test that IZiskejThemeLayer is removed."""
        from ziskej.theme.interfaces import \
            IZiskejThemeLayer
        from plone.browserlayer import utils
        self.assertNotIn(IZiskejThemeLayer, utils.registered_layers())
