// Charts
window.onload = function () {
    var chartTickets = document.getElementById('chart-tickets-area');

    // No charts
    if (!chartTickets) {
        return;
    }

    window.chartColors = {
        blue: 'rgb(34, 193, 237)',
        darkblue: 'rgb(65, 142, 186)',
        red: 'rgb(242, 106, 89)',
        orange: 'rgb(241, 155, 44)',
        green: 'rgb(25, 165, 92)',
        olivegreen: 'rgb(107, 142, 35)',
        violet: 'rgb(238,130,238)',
        grey: 'rgb(210, 214, 221)'
    };

    var configChartTickets = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    1,
                    1,
                    1,
                    1,
                    1,
                    1,
                    1,
                ],
                backgroundColor: [
                    window.chartColors.blue,
                    window.chartColors.orange,
                    window.chartColors.darkblue,
                    window.chartColors.green,
                    window.chartColors.olivegreen,
                    window.chartColors.grey,
                    window.chartColors.red,
                ]
            }],
            labels: [
                'Nová',
                'Ve zpracování',
                'Čeká na rozhodnutí',
                'Vyřízená',
                'Uzavřená',
                'Stornovaná',
                'Zamítnutá'
            ]
        },
        options: {
            responsive: true,
            legend: {
                position: 'right',
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    };

    var configChartSubtickets = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    1,
                    1,
                    1,
                    1,
                    1,
                    1,
                    1,
                    1,
                ],
                backgroundColor: [
                    window.chartColors.blue,
                    window.chartColors.darkblue,
                    window.chartColors.orange,
                    window.chartColors.green,
                    window.chartColors.violet,
                    window.chartColors.olivegreen,
                    window.chartColors.grey,
                    window.chartColors.red,
                ]
            }],
            labels: [
                'Nový',
                'Přijatý s podmínkou',
                'Ve zpracování',
                'Vyřízený - odeslaný',
                'Vyřízený - odeslaný zpět',
                'Uzavřený',
                'Stornovaný',
                'Odmítnutý'
            ]
        },
        options: {
            responsive: true,
            legend: {
                position: 'right',
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    };

    var chartTicketsContext = chartTickets.getContext('2d');
    window.chartTickets = new Chart(chartTicketsContext, configChartTickets);

    var chartSubtickets = document.getElementById('chart-subtickets-area');
    var chartSubticketsContext = chartSubtickets.getContext('2d');
    window.chartSubtickets = new Chart(chartSubticketsContext, configChartSubtickets);

    // Set charts links
    chartTickets.onclick = function(e) {
        var slice = window.chartTickets.getElementAtEvent(e);
        if (!slice.length) return;
        var idx = slice[0]._index;
        switch (idx) {
            case 0:
                window.open('tickets/listing_zk?filter_rs=created');
                break;
            case 1:
                window.open('tickets/listing_zk?filter_rs=assigned');
                break;
            case 2:
                window.open('tickets/listing_zk?filter_rs=pending');
                break;
            case 3:
                window.open('tickets/listing_zk?filter_rs=prepared');
                break;
            case 4:
                window.open('tickets/listing_zk?filter_rs=closed');
                break;
            case 5:
                window.open('tickets/listing_zk?filter_rs=cancelled');
                break;
            case 6:
                window.open('tickets/listing_zk?filter_rs=rejected');
                break;
        }
    };

    chartSubtickets.onclick = function(e) {
        var slice = window.chartSubtickets.getElementAtEvent(e);
        if (!slice.length) return;
        var idx = slice[0]._index;
        switch (idx) {
            case 0:
                window.open('tickets/listing_dk?filter_rs=queued');
                break;
            case 1:
                window.open('tickets/listing_dk?filter_rs=conditionally_accepted');
                break;
            case 2:
                window.open('tickets/listing_dk?filter_rs=accepted');
                break;
            case 3:
                window.open('tickets/listing_dk?filter_rs=sent');
                break;
            case 4:
                window.open('tickets/listing_dk?filter_rs=sent_back');
                break;
            case 5:
                window.open('tickets/listing_dk?filter_rs=closed');
                break;
            case 6:
                window.open('tickets/listing_dk?filter_rs=cancelled');
                break;
            case 7:
                window.open('tickets/listing_dk?filter_rs=refused');
                break;
        }
    };

    // Load data for charts
    function loadData() {
        // console.log('Loading data for charts.');
        $.ajax({
            // url: '++theme++ziskej-theme/src/chart-ajax-data-2.json',
            // url: 'ajax_overview_charts_data',
            url: 'ziskej/ajax_overview_charts_data',
            dataType: 'JSON',
            success: function (data) {
                // console.log('Processing data for charts.');
                if (data['status'] === "OK") {
                    for (i=0;i<7;i++) {
                        window.chartTickets.data.datasets[0].data[i] = data['chartTicketsData'][i];
                    };
                    window.chartTickets.update();
                    for (i=0;i<8;i++) {
                        window.chartSubtickets.data.datasets[0].data[i] = data['chartSubticketsData'][i];
                    };
                    window.chartSubtickets.update();
                    // console.log('Charts have been updated.');
                } else 
                if (data['status'] === "Error") {
                    console.log('Loading data for charts failed with error message: '+data['error_message']);
                }
            }
        });
    }
    setTimeout(loadData, 250);

};
