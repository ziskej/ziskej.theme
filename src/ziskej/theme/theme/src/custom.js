// Alert old browser
var $buoop = {
    required: {e:-3,i:-1,f:-3,o:-3,s:-3,c:-3},
    test: false,
    reminder: 168, // v hodinách, týden 168
    text: '<strong>Používáte prohlížeč s omezenou podporou, funkčnost může být omezena.</strong><br>Doporučujeme používat podporované prohlížeče (Chrome, Firefox, Safari) v posledních verzích. Více o podpoře prohlížečů na <a href="https://browser-update.org/update-browser.html" target="_blank">browser-update.org</a>.',
    insecure:true,
    api:2020.10,
    jsshowurl:"/++theme++ziskej-theme/src/update.show.min.js"
    };
function $buo_f(){
var e = document.createElement("script");
              e.src = "/++theme++ziskej-theme/src/update.min.js";
              document.body.appendChild(e);
         };
try {document.addEventListener("DOMContentLoaded", $buo_f, false)}
catch(e){window.attachEvent("onload", $buo_f)}
// Alert old browser


$(document).ready(function() {

    //sweetalert
    $(".btn-get-info").click(function() {
        swal({
            text: $(this).attr('title'
            )});
    });


    // Cookie bar
    //vytvoření cookie
    function setCookie(lawCookie,value) {
        var d = new Date();
        d.setTime(d.getTime() + (365*24*60*60*1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = lawCookie + "=" + value + ";" + expires + ";path=/";
    }

    //získání informací o cookies
    function getCookie(lawConfirm) {
        var name = lawConfirm + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //kontrola jestli je cookie povrzený
    function checkCookie() {
        var lawConfirm=getCookie("lawCookie");
        if (lawConfirm == "true") {

        }
        else {
            $("#cookie-law").show();
            setCookie("lawCookie", "false");
        }
    }


    $("#confirm-cookie-law").click(function() {
      setCookie("lawCookie", "true");
      $("#cookie-law").hide();
    });

    //volání funkce pro ověření cookie
    checkCookie();
    // Cookie bar


    // Enable Tooltip
    $('[data-toggle="tooltip"]').tooltip();

    // Drag&Drop library
    var adjustment;

    $("ol.drag-library").sortable({
      group: 'simple_with_animation',
      pullPlaceholder: false,
      // animation on drop
      onDrop: function  ($item, container, _super) {
        var $clonedItem = $('<li/>').css({height: 0});
        $item.before($clonedItem);
        $clonedItem.animate({'height': $item.height()});

        $item.animate($clonedItem.position(), function  () {
          $clonedItem.detach();
          _super($item, container);
        });
      },

      // set $item relative to cursor position
      onDragStart: function ($item, container, _super) {
        var offset = $item.offset(),
            pointer = container.rootGroup.pointer;

        adjustment = {
          left: pointer.left - offset.left,
          top: pointer.top - offset.top
        };

        _super($item, container);
      },
      onDrag: function ($item, position) {
        $item.css({
          left: position.left - adjustment.left,
          top: position.top - adjustment.top
        });
      }
    });

    // DatePicker
    $(".datepicker-wrapper").click(function(){
        $(this).datepicker({
            autoclose: true,
            todayHighlight: true
        }).datepicker('show',new Date());
    })

    // Instance
    if ($("#body-css").hasClass("instance-prod")) {
        $("body").addClass("instance-prod");
    }
    if ($("#body-css").hasClass("instance-demo")) {
        $("body").addClass("instance-demo");
    }
    if ($("#body-css").hasClass("instance-test")) {
        $("body").addClass("instance-test");
    }
    if ($("#body-css").hasClass("instance-dev")) {
        $("body").addClass("instance-dev");
    }
    if ($("#body-css").hasClass("instance-development")) {
        $("body").addClass("instance-development");
    }
    if ($("#body-css").hasClass("instance-not-prod")) {
        $("body").addClass("instance-not-prod");
    }

    // Texty

    // Editace
    $("body.template-edit .documentFirstHeading").text("Editace");

    // Log in
    $("body.template-login_form #login-form label[for='__ac_name']").text("Uživatelské jméno");
    $("body.template-login_form #login-form label[for='__ac_password']").text("Heslo");

    // Logout
    $("body.template-logged_out .documentFirstHeading").text("Byli jste odhlášeni");
    $("body.template-logged_out .documentDescription").text("Můžete se znovu přihlásit.");
    $("body.template-logged_out #login-form label[for='__ac_name']").text("Uživatelské jméno");
    $("body.template-logged_out #login-form label[for='__ac_password']").text("Heslo");

    // Confirming User Action
    $("body.template-confirm-action .documentFirstHeading").text("Potvrzení akce uživatele.");
    $("body.template-confirm-action .documentDescription").text("Potvrďte prosím, že chcete provést tut akci.");
    $("body.template-confirm-action #content-core > p.discreet").text("Pozor, je možné, že někdo se snaží zneužít vaše přihlášení.  Potvrďte, že jste právě na tomto webu provedli akci a že sem nejste odkázáni z jiného webu nebo e-mailu.");
    $("body.template-confirm-action form dt").text("Původní adresa");
    $("body.template-confirm-action input[name='form.button.confirm']").attr('value', 'Potvrdit akci')


    // global_statusmessage

    var message = "";
    var message_new = "";

    message = "\n        <strong>Info</strong>\n        Welcome! You are now logged in.\n    "
    message_new = "<strong>Informace</strong> Jste přihlášeni!";
    if ($("#global_statusmessage .portalMessage").html() == message) {
        $("#global_statusmessage .portalMessage").html(message_new);
    };

    message = "\n        <strong>Info</strong>\n        Changes saved\n    "
    message_new = "<strong>Informace</strong> Změny jsou uloženy";
    if ($("#global_statusmessage .portalMessage").html() == message) {
        $("#global_statusmessage .portalMessage").html(message_new);
    };

    if ($("#global_statusmessage .portalMessage.info strong").text() == "Info") {
        $("#global_statusmessage .portalMessage.info strong").text("Informace");
    };

    if ($("#global_statusmessage .portalMessage.warning strong").text() == "Warning") {
        $("#global_statusmessage .portalMessage.warning strong").text("Varování");
    };

    $("#global_statusmessage").show(1250);

    // Sticky footer
    function stickyFooter() {

      var heightWin = $(window).height();
      var heightBody = $("body").height();

      if (heightWin > heightBody) {
        var differenceHeight = heightWin - heightBody;
        $("footer").css("padding-top",differenceHeight + "px");
      } else {
        $("footer").css("padding-top","0px");
      }
    }
    stickyFooter();

    // Funkce pro resize
    $(window).resize(function() {
      stickyFooter()
    });
});